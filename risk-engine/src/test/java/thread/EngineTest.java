package thread;

import io.quarkus.test.junit.QuarkusTest;
import gson.Portfolio;
import gson.RiskProfile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import gson.ScannedPortfolio;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class EngineTest {

    Portfolio por_1 = new Portfolio("POR_1", "L", 3d);
    Portfolio por_2 = new Portfolio("POR_2", "L", 6d);
    Portfolio por_3 = new Portfolio("POR_3", "L", 8d);
    Portfolio por_4 = new Portfolio("POR_4", "L", 5d);
    Portfolio por_5 = new Portfolio("POR_5", "L", 2d);
    Portfolio por_6 = new Portfolio("POR_6", "L", 1d);
    Portfolio por_7 = new Portfolio("POR_7", "M", 7d);
    Portfolio por_8 = new Portfolio("POR_8", "M", 6.5d);
    Portfolio por_9 = new Portfolio("POR_9", "M", 4d);
    Portfolio por_10 = new Portfolio("POR_10", "M", 3.5d);
    Portfolio por_11 = new Portfolio("POR_11", "M", 7.421d);
    Portfolio por_12 = new Portfolio("POR_12", "M", 10d);
    Portfolio por_13 = new Portfolio("POR_13", "M", 3.1d);
    Portfolio por_14 = new Portfolio("POR_14", "H", 5.5d);
    Portfolio por_15 = new Portfolio("POR_15", "H", 6.5d);
    Portfolio por_16 = new Portfolio("POR_16", "H", 7d);
    Portfolio por_17 = new Portfolio("POR_17", "H", 2d);
    Portfolio por_18 = new Portfolio("POR_18", "H", 8d);
    Portfolio por_19 = new Portfolio("POR_19", "H", 7.5d);
    Portfolio por_20 = new Portfolio("POR_20", "H", 9.2d);

    RiskProfile riskProfile_L = new RiskProfile("L", "Low", 1d, 4d, .2);
    RiskProfile riskProfile_M = new RiskProfile("M", "Medium", 4d, 7d, .15);
    RiskProfile riskProfile_H = new RiskProfile("H", "High", 7d, 10d, .25);

    @Test
    @DisplayName("Ensure that there are no memory volatility errors by running the engine several times.")
    public void ensureEngineRunsWithoutMemoryVolatilityIssues () throws InterruptedException {
        List<RiskProfile> riskProfiles = Arrays.asList(riskProfile_L, riskProfile_M, riskProfile_H);
        List<Portfolio> portfolios = Arrays.asList(por_1, por_2, por_3, por_4, por_5, por_6, por_7, por_8, por_9, por_10,
                por_11, por_12, por_13, por_14, por_15, por_16, por_17, por_18, por_19, por_20);

        for(int x = 0; x < 10; x++){
            Engine engine = new Engine(riskProfiles, portfolios);
            List<ScannedPortfolio> results = engine.processPortfolios();
            assertEquals(results.size(), portfolios.size());
        }
    }

    @Test
    @DisplayName("Ensure the engine is capable of recording CLEAR scan result types.")
    public void ensureEngineReturnsCLEARResultTypes() throws InterruptedException {
        List<RiskProfile> riskProfiles = Collections.singletonList(riskProfile_L);
        List<Portfolio> portfolios = Collections.singletonList(por_1);

        Engine engine = new Engine(riskProfiles, portfolios);
        List<ScannedPortfolio> results = engine.processPortfolios();
        ScannedPortfolio result = results.get(0);

        assertEquals(results.size(), portfolios.size());
        assertEquals(result.getPortfolioId(), por_1.getPortfolioName());
        assertEquals(result.getPortfolioStatus(), Status.CLEAR.name());
    }

    @Test
    @DisplayName("Ensure the engine is capable of recording WARNING scan result types when approaching the upper risk limit.")
    public void ensureEngineReturnsUpperLimitWARNINGResultTypes() throws InterruptedException {
        List<RiskProfile> riskProfiles = Collections.singletonList(riskProfile_M);
        List<Portfolio> portfolios = Collections.singletonList(por_8);

        Engine engine = new Engine(riskProfiles, portfolios);
        List<ScannedPortfolio> results = engine.processPortfolios();
        ScannedPortfolio result = results.get(0);

        assertEquals(results.size(), portfolios.size());
        assertEquals(result.getPortfolioId(), por_8.getPortfolioName());
        assertEquals(result.getPortfolioStatus(), Status.WARNING.name());
        assertEquals(String.format("Risk score of %s is approaching the upper risk limit of %s according to the tolerance level of %s%%.",
                por_8.getPortfolioRiskScore(), riskProfile_M.getMaximumRiskLimit(), riskProfile_M.getWarningThresholdPercentage() * 100), result.getDescription());
    }

    @Test
    @DisplayName("Ensure the engine is capable of recording WARNING scan result types when approaching the lower risk limit.")
    public void ensureEngineReturnsLowerLimitWARNINGResultTypes() throws InterruptedException {
        List<RiskProfile> riskProfiles = Collections.singletonList(riskProfile_L);
        List<Portfolio> portfolios = Collections.singletonList(por_6);

        Engine engine = new Engine(riskProfiles, portfolios);
        List<ScannedPortfolio> results = engine.processPortfolios();
        ScannedPortfolio result = results.get(0);

        assertEquals(results.size(), portfolios.size());
        assertEquals(result.getPortfolioId(), por_6.getPortfolioName());
        assertEquals(result.getPortfolioStatus(), Status.WARNING.name());
        assertEquals(String.format("Risk score of %s is approaching the lower risk limit of %s according to the tolerance level of %s%%.",
                por_6.getPortfolioRiskScore(), riskProfile_L.getMinimumRiskLimit(), riskProfile_L.getWarningThresholdPercentage() * 100), result.getDescription());
    }

    @Test
    @DisplayName("Ensure the engine is capable of recording ALERT scan result types when above the upper risk limit.")
    public void ensureEngineReturnsALERTResultTypesWhenTooHigh() throws InterruptedException {
        List<RiskProfile> riskProfiles = Collections.singletonList(riskProfile_L);
        List<Portfolio> portfolios = Collections.singletonList(por_2);

        Engine engine = new Engine(riskProfiles, portfolios);
        List<ScannedPortfolio> results = engine.processPortfolios();
        ScannedPortfolio result = results.get(0);

        assertEquals(results.size(), portfolios.size());
        assertEquals(result.getPortfolioId(), por_2.getPortfolioName());
        assertEquals(result.getPortfolioStatus(), Status.ALERT.name());
        assertEquals(String.format("Risk score of %s exceeds the upper risk limit of %s.",
                por_2.getPortfolioRiskScore(), riskProfile_L.getMaximumRiskLimit()), result.getDescription());
    }

    @Test
    @DisplayName("Ensure the engine is capable of recording ALERT scan result types when below the lower risk limit.")
    public void ensureEngineReturnsALERTResultTypesWhenTooLow() throws InterruptedException {
        List<RiskProfile> riskProfiles = Collections.singletonList(riskProfile_M);
        List<Portfolio> portfolios = Collections.singletonList(por_10);

        Engine engine = new Engine(riskProfiles, portfolios);
        List<ScannedPortfolio> results = engine.processPortfolios();
        ScannedPortfolio result = results.get(0);

        assertEquals(results.size(), portfolios.size());
        assertEquals(result.getPortfolioId(), por_10.getPortfolioName());
        assertEquals(result.getPortfolioStatus(), Status.ALERT.name());
        assertEquals(String.format("Risk score of %s is below the lower limit of %s.",
                por_10.getPortfolioRiskScore(), riskProfile_M.getMinimumRiskLimit()), result.getDescription());
    }
}
