package database.requests;

import com.google.gson.Gson;
import com.squareup.okhttp.*;

import java.io.IOException;

public class HttpRequestCreator {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();
    Gson gson = new Gson();

    public boolean post(String url, Object obj) throws IOException {
        String json = gson.toJson(obj);
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = executor(client.newCall(request));
        return response.isSuccessful();
    }
    public Response executor (Call call) throws IOException {
        return call.execute();
    }
}
