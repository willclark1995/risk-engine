package gson;

public class RiskProfile {

    private String riskProfile;
    private String riskProfileDefinition;
    private Double minimumRiskLimit;
    private Double maximumRiskLimit;
    private Double warningThresholdPercentage;

    public RiskProfile(String riskProfile, String riskProfileDefinition, Double minimumRiskLimit, Double maximumRiskLimit, Double warningThresholdPercentage) {
        this.riskProfile = riskProfile;
        this.riskProfileDefinition = riskProfileDefinition;
        this.maximumRiskLimit = maximumRiskLimit;
        this.minimumRiskLimit = minimumRiskLimit;
        this.warningThresholdPercentage = warningThresholdPercentage;
    }

    public String getRiskProfile() {
        return riskProfile;
    }
    public void setRiskProfile(String riskProfile) {
        this.riskProfile = riskProfile;
    }

    public String getRiskProfileDefinition() {
        return riskProfileDefinition;
    }
    public void setRiskProfileDefinition(String riskProfileDefinition) {
        this.riskProfileDefinition = riskProfileDefinition;
    }

    public Double getMaximumRiskLimit() {
        return maximumRiskLimit;
    }
    public void setMaximumRiskLimit(Double maximumRiskLimit) {
        this.maximumRiskLimit = maximumRiskLimit;
    }

    public Double getMinimumRiskLimit() {
        return minimumRiskLimit;
    }
    public void setMinimumRiskLimit(Double minimumRiskLimit) {
        this.minimumRiskLimit = minimumRiskLimit;
    }

    public Double getWarningThresholdPercentage() {
        return warningThresholdPercentage;
    }
    public void setWarningThresholdPercentage(Double warningThresholdPercentage) {
        this.warningThresholdPercentage = warningThresholdPercentage;
    }
}
