package gson;

import java.util.List;

public class ScannedPortfolios {

    private List<ScannedPortfolio> scannedPortfolios;

    public ScannedPortfolios(List<ScannedPortfolio> scannedPortfolios) {
        this.scannedPortfolios = scannedPortfolios;
    }

    public List<ScannedPortfolio> getPortfolioResults() {
        return scannedPortfolios;
    }
}
