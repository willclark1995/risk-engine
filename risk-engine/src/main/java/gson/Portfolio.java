package gson;

public class Portfolio {

    private String portfolioName;
    private String portfolioRiskProfile;
    private Double portfolioRiskScore;

    public Portfolio(String portfolioName, String portfolioRiskProfile, Double portfolioRiskScore) {
        this.portfolioName = portfolioName;
        this.portfolioRiskProfile = portfolioRiskProfile;
        this.portfolioRiskScore = portfolioRiskScore;
    }

    public String getPortfolioName() {
        return portfolioName;
    }
    public void setPortfolioName(String portfolioName) {
        this.portfolioName = portfolioName;
    }

    public String getPortfolioRiskProfile() {
        return portfolioRiskProfile;
    }
    public void setPortfolioRiskProfile(String portfolioRiskProfile) {
        this.portfolioRiskProfile = portfolioRiskProfile;
    }

    public Double getPortfolioRiskScore() {
        return portfolioRiskScore;
    }
    public void setPortfolioRiskScore(Double portfolioRiskScore) {
        this.portfolioRiskScore = portfolioRiskScore;
    }

}
