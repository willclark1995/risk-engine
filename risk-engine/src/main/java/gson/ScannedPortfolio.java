package gson;

public class ScannedPortfolio {

    private String portfolioName;
    private String status;
    private String description;
    private String date;

    public ScannedPortfolio(String portfolioName, String status, String description, String date) {
        this.portfolioName = portfolioName;
        this.status = status;
        this.description = description;
        this.date = date;
    }

    public String getPortfolioId() {
        return portfolioName;
    }
    public void setPortfolioId(String portfolioId) {
        this.portfolioName = portfolioId;
    }

    public String getPortfolioStatus() {
        return status;
    }
    public void setPortfolioStatus(String portfolioStatus) {
        this.status = portfolioStatus;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getScanTimestamp() {
        return date;
    }
    public void setScanTimestamp(String date) {
        this.date = date;
    }
}
