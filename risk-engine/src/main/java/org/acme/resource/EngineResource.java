package org.acme.resource;

import database.requests.HttpRequestCreator;
import gson.PortfolioRiskProfilePair;
import gson.ScannedPortfolio;
import gson.ScannedPortfolios;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import thread.Engine;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path("/engine")
public class EngineResource {

    @ConfigProperty(name="results.endpoint")
    String resultsEndpoint;

    HttpRequestCreator hrc = new HttpRequestCreator();

    @POST
    public Response.Status runEngine(PortfolioRiskProfilePair pair) throws InterruptedException, IOException {
        Engine e = new Engine(pair.getRiskProfiles(), pair.getPortfolios());
        List<ScannedPortfolio> results = e.processPortfolios();
        hrc.post(resultsEndpoint, new ScannedPortfolios(results));
        return Response.Status.OK;
    }
}
