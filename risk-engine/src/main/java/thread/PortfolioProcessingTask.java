package thread;

import gson.Portfolio;
import gson.RiskProfile;
import gson.ScannedPortfolio;
import java.time.LocalDateTime;
import java.util.List;

public class PortfolioProcessingTask implements Runnable {

    private String name;
    private Portfolio portfolio;
    private List<RiskProfile> riskProfiles;
    private volatile Double riskScore = 0d;

    public PortfolioProcessingTask(String name, Portfolio portfolio, List<RiskProfile> riskProfiles) {
        this.name = name;
        this.portfolio = portfolio;
        this.riskProfiles = riskProfiles;
    }

    public void run() {

        String portfolioId = portfolio.getPortfolioName();
        Status portfolioStatus;
        String description;

        riskScore = portfolio.getPortfolioRiskScore();
        RiskProfile portfolioRiskProfile = riskProfiles.stream().filter(x -> portfolio.getPortfolioRiskProfile().equals(x.getRiskProfile())).findAny().orElse(null);

        if (portfolioRiskProfile.getMaximumRiskLimit() < riskScore) {
            description = String.format("Risk score of %s exceeds the upper risk limit of %s.", riskScore, portfolioRiskProfile.getMaximumRiskLimit());
            portfolioStatus = Status.ALERT;
        } else if (portfolioRiskProfile.getMinimumRiskLimit() > riskScore) {
            description = String.format("Risk score of %s is below the lower limit of %s.", riskScore, portfolioRiskProfile.getMinimumRiskLimit());
            portfolioStatus = Status.ALERT;
        } else if (portfolioRiskProfile.getMaximumRiskLimit() * (1 - portfolioRiskProfile.getWarningThresholdPercentage()) < riskScore) {
            description = String.format("Risk score of %s is approaching the upper risk limit of %s according to the tolerance level of %s%%.", riskScore, portfolioRiskProfile.getMaximumRiskLimit(), portfolioRiskProfile.getWarningThresholdPercentage() * 100);
            portfolioStatus = Status.WARNING;
        } else if (portfolioRiskProfile.getMinimumRiskLimit() * (1 + portfolioRiskProfile.getWarningThresholdPercentage()) > riskScore) {
            description = String.format("Risk score of %s is approaching the lower risk limit of %s according to the tolerance level of %s%%.", riskScore, portfolioRiskProfile.getMinimumRiskLimit(), portfolioRiskProfile.getWarningThresholdPercentage() * 100);
            portfolioStatus = Status.WARNING;
        } else {
           description = "Portfolio is satisfactory.";
           portfolioStatus = Status.CLEAR;
        }

        ScannedPortfolio ps = new ScannedPortfolio(portfolioId, portfolioStatus.toString(), description, LocalDateTime.now().toString());
        Engine.scannedPortfolios.add(ps);
    }
}
