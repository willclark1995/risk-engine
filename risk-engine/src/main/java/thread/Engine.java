package thread;

import gson.Portfolio;
import gson.RiskProfile;
import gson.ScannedPortfolio;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Engine {

    private final int NUMBER_OF_THREADS = 4;
    private List<RiskProfile> riskProfiles;
    private List<Portfolio> portfolios;
    private ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    static List<ScannedPortfolio> scannedPortfolios = Collections.synchronizedList(new ArrayList<>());

    public Engine(List<RiskProfile> riskProfiles, List<Portfolio> portfolios) {
        scannedPortfolios.clear();
        this.riskProfiles = riskProfiles;
        this.portfolios = portfolios;
    }

    public List<ScannedPortfolio> processPortfolios() throws InterruptedException {

        portfolios.forEach(p -> {
            PortfolioProcessingTask portfolioProcessingTask = new PortfolioProcessingTask("Task " + p.getPortfolioName(), p, riskProfiles);
            executor.execute(portfolioProcessingTask);
        });
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.MINUTES);

        return scannedPortfolios;
    }
}
