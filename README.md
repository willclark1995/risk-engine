# Risk Engine README #

This README will explain how to set up and run the risk engine on your machine.

### What is the aim of this project? ###

The code enclosed in this project prototypes a multi-threaded and thread-safe portfolio risk calculating application, written using the Quarkus framework running over a Postgres database. The application is developed with a microservice architecture consisting of two microservices: the risk-engine API (which performs the calculations and logic detailed below) and the database-api (the API responsible for communicating with the Postgres database).

Given a set of portfolios, accounts and instruments, the application calculates the risk score of the portfolio's according to the equation:

* ![risk_equation](https://latex.codecogs.com/png.image?\dpi{110}%20\mathbf{risk}%20=%20r_{1}\ast%20w_{1}%20+%20r_{1}\ast%20%20w_{1}%20...%20+%20r_{n}\ast%20%20w_{n})

where *r* and *w* are the risk score and weightings (in terms of total instrument value) of a portfolio's instruments.

The calculated value of __risk__ is then compared to a risk profile which dictates the portfolio's: 

* Maximum allowed risk limit
* Minimum allowed risk limit
* Warning threshold - the % from the maximum and minimum allowed a portfolio's risk score can be before raising a Warning (see below).

If the portfolio risk score is above the maximum limit or below the minimum limit: an __ALERT__ is recorded.
If the portfolio risk score is breaching the warning threshold, and is close to either the maximum or minimum risk limit: a __WARNING__ is recorded.
_e.g. If a portfolio has a risk score of 7.5, and it's risk profile states a maximum risk limit of 8 with a Warning Threshold of 10%, since the risk score is greater than 7.2 a Warning will be recorded._
If the portfolio risk score is not in violation of either of the above, a __CLEAR__ is recorded.

Narratives are also written alongside each record, explaining the reason for the Alert or Warning.

### Prerequisites ###

* JDK 11+ installed with JAVA_HOME configured appropriately
* Apache Maven 3.8.1+
* Docker Desktop (preferably the latest version)
* A command shell/ bash terminal

### Running the application ###

__First time run only:__ 

* From the main directory, navigate to the database-api directory and package the microservice using: `mvnw package`, change directory to the risk-engine microservice and do the same using `./risk-engine/mvnw package`.
* Then, create the docker images using the commands: `docker build -f src/main/docker/Dockerfile.jvm -t quarkus/database-api-jvm .` and `docker build -f src/main/docker/Dockerfile.jvm -t quarkus/risk-engine-jvm .`

__Thereafter:__

* A docker-compose.yaml exists in the main project directory which runs the two images detailed above and the Postgres image. To persist data in the Postgres container even after you shut down Docker, uncomment the `./postgres-data` volume in the docker-compose file. The database is populated with dummy data specified in the sql/fill_tables.sql file.
* The database-api microservice provides a very basic UI avaiable at the following URL's based on your local machine's IP address:
* * http://YOUR_IP_HERE:8080/portfolio-account-mapping.html where you can provide information regarding what account is in which portfolio.
* * http://YOUR_IP_HERE:8080/account-instrument-mapping.html where you can provide information regarding what instrument is in which account.
* * http://YOUR_IP_HERE:8080/instrument.html where you can provide information regarding different instruments.
* * http://YOUR_IP_HERE:8080/risk-profile-api.html where you can provide risk profile information.
* * http://YOUR_IP_HERE:8080/results.html where you can run the risk engine, and view the risk engine's results.


#### Obstacles faced during this project ####

Race conditions presented complications during the development of this project. The risk-engine microservice used a hard-coded number of threads (4), with each thread being allocated a workload to calculate the risk score and result (Clear, Warning or Alert) of a portfolio. These threads would then update a shared List with the result they calculated.

This meant that the application was prone to read-modify-write race conditions - ultimately meaning that given a constant input, the risk engine did __not__ provide a constant output. This issue was fixed by wrapping the List in a `Collections.synchronizedList(...)` method which ensured that multiple calls into the new synchronized collection were performed as a single, atomic operation. Meaning that when a thread is performing an operation on the List, no other thread can access it.