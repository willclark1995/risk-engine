package database.requests;

import com.squareup.okhttp.*;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.Portfolio;
import org.acme.RiskProfile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@QuarkusTest
public class HttpRequestCreatorTest {

    private final String mockURL = "https://url.com";
    private final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final HttpRequestCreator hrc = spy(new HttpRequestCreator());
    private final Object obj = new Object();
    private final Request mockRequest = new Request.Builder().url(mockURL).build();

    @Test
    @DisplayName("Ensure post method returns true following successful request.")
    public void ensurePostReturnsTrueFollowingSuccessfulRequest () throws IOException {
        Response mockResponse = new Response.Builder()
                .request(mockRequest)
                .protocol(Protocol.HTTP_2)
                .code(200)
                .body(ResponseBody.create(JSON, "{}"))
                .build();

        doReturn(mockResponse).when(hrc).executor(any());
        assertTrue(hrc.post(mockURL, obj));
    }

    @Test
    @DisplayName("Ensure post method returns false following unsuccessful request.")
    public void ensurePostReturnsFalseFollowingUnsuccessfulRequest () throws IOException {
        Response mockResponse = new Response.Builder()
                .request(mockRequest)
                .protocol(Protocol.HTTP_2)
                .code(401)
                .body(ResponseBody.create(JSON, "{}"))
                .build();

        doReturn(mockResponse).when(hrc).executor(any());
        assertFalse(hrc.post(mockURL, obj));
    }

    @Test
    @DisplayName("Ensure get method is truly generic and can return different object types.")
    public void ensureGetReturnsMultipleTypes () throws IOException {
        RiskProfile riskProfile = new RiskProfile();
        Portfolio portfolio = new Portfolio();

        Response mockResponse = new Response.Builder()
                .request(mockRequest)
                .protocol(Protocol.HTTP_2)
                .code(200)
                .body(ResponseBody.create(JSON, "{}"))
                .build();
        doReturn(mockResponse).when(hrc).executor(any());
        assertEquals(riskProfile.getClass(), hrc.get(mockURL, RiskProfile.class).getClass());

        Response mockResponse_2 = new Response.Builder()
                .request(mockRequest)
                .protocol(Protocol.HTTP_2)
                .code(200)
                .body(ResponseBody.create(JSON, "{}"))
                .build();
        doReturn(mockResponse_2).when(hrc).executor(any());
        assertEquals(portfolio.getClass(), hrc.get(mockURL, Portfolio.class).getClass());
    }
}
