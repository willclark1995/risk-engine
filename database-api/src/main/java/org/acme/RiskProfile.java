package org.acme;

import database.queries.RiskProfileQueries;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;

public class RiskProfile {

    private String riskProfile;
    private String riskProfileDefinition;
    private Double minimumRiskLimit;
    private Double maximumRiskLimit;
    private Double warningThresholdPercentage;

    public RiskProfile(String riskProfile, String riskProfileDefinition, Double minimumRiskLimit, Double maximumRiskLimit, Double warningThresholdPercentage) {
        this.riskProfile = riskProfile;
        this.riskProfileDefinition = riskProfileDefinition;
        this.maximumRiskLimit = maximumRiskLimit;
        this.minimumRiskLimit = minimumRiskLimit;
        this.warningThresholdPercentage = warningThresholdPercentage;
    }

    public RiskProfile() {
    }

    public String getRiskProfile() {
        return riskProfile;
    }
    public void setRiskProfile(String riskProfile) {
        this.riskProfile = riskProfile;
    }

    public String getRiskProfileDefinition() {
        return riskProfileDefinition;
    }
    public void setRiskProfileDefinition(String riskProfileDefinition) {
        this.riskProfileDefinition = riskProfileDefinition;
    }

    public Double getMaximumRiskLimit() {
        return maximumRiskLimit;
    }
    public void setMaximumRiskLimit(Double maximumRiskLimit) {
        this.maximumRiskLimit = maximumRiskLimit;
    }

    public Double getMinimumRiskLimit() {
        return minimumRiskLimit;
    }
    public void setMinimumRiskLimit(Double minimumRiskLimit) {
        this.minimumRiskLimit = minimumRiskLimit;
    }

    public Double getWarningThresholdPercentage() {
        return warningThresholdPercentage;
    }
    public void setWarningThresholdPercentage(Double warningThresholdPercentage) {
        this.warningThresholdPercentage = warningThresholdPercentage;
    }

    public static Multi<RiskProfile> findAll(PgPool client) {

        return client.query(RiskProfileQueries.selectAll()).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(RiskProfile::from);
    }

    public static Uni<RiskProfile> findByName(PgPool client, String riskProfileName) {
        return client.query(RiskProfileQueries.findByName(riskProfileName)).execute()
                .onItem()
                .transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }

    public static Uni<String> save(PgPool client, String riskProfile, String riskProfileDescription, Double minimumRiskLimit, Double maximumRiskLimit, Double warningThresholdPercentage) {
        return client
                .preparedQuery(RiskProfileQueries.save(riskProfile, riskProfileDescription, minimumRiskLimit, maximumRiskLimit, warningThresholdPercentage))
                .execute()
                .onItem()
                .transform(m -> m.iterator().next().getString("risk_profile"));
    }

    public static Uni<Boolean> delete(PgPool client, String riskProfile) {
        return client
                .preparedQuery(RiskProfileQueries.delete(riskProfile))
                .execute()
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    private static RiskProfile from(Row row) {
        return new RiskProfile(row.getString("risk_profile"), row.getString("risk_profile_description"), row.getDouble("minimum_risk_limit"), row.getDouble("maximum_risk_limit"), row.getDouble("warning_threshold_percentage"));
    }
}
