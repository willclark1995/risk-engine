package org.acme;

import database.queries.PortfoliosQueries;
import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;

public class Portfolio {

    private String portfolioName;
    private String portfolioRiskProfile;
    private Double portfolioRiskScore;

    public Portfolio(String portfolioName, String portfolioRiskProfile, Double portfolioRiskScore) {
        this.portfolioName = portfolioName;
        this.portfolioRiskProfile = portfolioRiskProfile;
        this.portfolioRiskScore = portfolioRiskScore;
    }

    public Portfolio() {
    }

    public String getPortfolioName() {
        return portfolioName;
    }
    public void setPortfolioName(String portfolioName) {
        this.portfolioName = portfolioName;
    }

    public String getPortfolioRiskProfile() {
        return portfolioRiskProfile;
    }
    public void setPortfolioRiskProfile(String portfolioRiskProfile) {
        this.portfolioRiskProfile = portfolioRiskProfile;
    }

    public Double getPortfolioRiskScore() {
        return portfolioRiskScore;
    }
    public void setPortfolioRiskScore(Double portfolioRiskScore) {
        this.portfolioRiskScore = portfolioRiskScore;
    }

    public static Multi<Portfolio> findAll(PgPool client) {
        return client.query(PortfoliosQueries.getPortfolios()).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(Portfolio::from);
    }

    private static Portfolio from(Row row) {
        return new Portfolio(row.getString("portfolio_name"), row.getString("portfolio_risk_profile"), row.getDouble("portfolio_risk_score"));
    }
}
