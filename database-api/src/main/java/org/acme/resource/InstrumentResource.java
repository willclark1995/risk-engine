package org.acme.resource;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.Instrument;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/instrument")
public class InstrumentResource {

    @Inject
    PgPool client;

    @GET
    public Multi<Instrument> get() {
        return Instrument.findAll(client);
    }

    @POST
    public Uni<Response> create(Instrument instrument) {
        return Instrument.save(client, instrument.getInstrumentName(), instrument.getInstrumentValue(), instrument.getRiskScore())
                .onItem()
                .transform(name -> URI.create("/instrument/" + name))// I changed this from /instruments/ to /instrument/ , if it stops working change it back.
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("{name}")
    public Uni<Response> delete(@PathParam("name") String name) {
        return Instrument.delete(client, name)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    @GET
    @Path("{name}")
    public Uni<Instrument> get(@PathParam("name") String name) {
        return Instrument.findByName(client, name);
    }
}
