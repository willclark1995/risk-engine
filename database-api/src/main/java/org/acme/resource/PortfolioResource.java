package org.acme.resource;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.Portfolio;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/portfolios")
public class PortfolioResource {

    @Inject
    PgPool client;

    @GET
    public Multi<Portfolio> get() { return Portfolio.findAll(client); }

}
