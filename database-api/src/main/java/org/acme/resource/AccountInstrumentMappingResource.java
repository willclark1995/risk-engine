package org.acme.resource;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.AccountInstrumentMapping;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/account-instrument-mapping")
public class AccountInstrumentMappingResource {

    @Inject
    PgPool client;

    @GET
    public Multi<AccountInstrumentMapping> get() { return AccountInstrumentMapping.findAll(client); }

    @POST
    public Uni<Response> create(AccountInstrumentMapping accountInstrumentMapping) {
        return AccountInstrumentMapping.save(client, accountInstrumentMapping.getAccountName(), accountInstrumentMapping.getInstrumentName(), accountInstrumentMapping.getInstrumentQuantity())
                .onItem()
                .transform(name -> URI.create("/account-instrument-mapping/" + name))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("{accountName}/{instrumentName}")
    public Uni<Response> delete(@PathParam("accountName") String accountName, @PathParam("instrumentName") String instrumentName) {
        return AccountInstrumentMapping.delete(client, accountName, instrumentName)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    @GET
    @Path("{name}")
    public Uni<AccountInstrumentMapping> get(@PathParam("name") String name) {
        { return AccountInstrumentMapping.findByName(client, name); }
    }
}
