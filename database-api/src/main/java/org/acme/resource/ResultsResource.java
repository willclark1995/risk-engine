package org.acme.resource;

import database.queries.ScanResultQueries;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.ScannedPortfolio;
import gson.ScannedPortfolios;
import main.Main;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;

@Path("/results")
public class ResultsResource {

    @Inject
    PgPool client;

    @GET
    public Multi<ScannedPortfolio> get() { return ScannedPortfolio.findAll(client); }

    @POST
    public Uni<Response> create(ScannedPortfolios results) {
        return ScannedPortfolio.save(client, ScanResultQueries.insertResultsIntoTable(results.getPortfolioResults()))
                .onItem()
                .transform(name -> URI.create("/results/" + name))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @POST
    @Path("/run")
    public boolean run() throws IOException {
        Main m = new Main();
        m.setClient(client);
        return m.processPortfolios();
    }
}
