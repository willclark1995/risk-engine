package org.acme.resource;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.RiskProfile;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/risk-profile")
public class RiskProfileResource {

    @Inject
    PgPool client;

    @GET
    public Multi<RiskProfile> get() { return RiskProfile.findAll(client); }

    @POST
    public Uni<Response> create(RiskProfile riskProfile) {
        return RiskProfile.save(client, riskProfile.getRiskProfile(), riskProfile.getRiskProfileDefinition(), riskProfile.getMinimumRiskLimit(), riskProfile.getMaximumRiskLimit(), riskProfile.getWarningThresholdPercentage())
                .onItem()
                .transform(name -> URI.create("/risk-profile/" + name))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("{riskProfile}")
    public Uni<Response> delete(@PathParam("riskProfile") String riskProfile) {
        return RiskProfile.delete(client, riskProfile)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    @GET
    @Path("{name}")
    public Uni<RiskProfile> get(@PathParam("name") String name) {
        { return RiskProfile.findByName(client, name); }
    }

}
