package org.acme.resource;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.PortfolioAccountMapping;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/portfolio-account-mapping")
public class PortfolioAccountMappingResource {

    @Inject
    PgPool client;

    @GET
    public Multi<PortfolioAccountMapping> get() { return PortfolioAccountMapping.findAll(client); }

    @POST
    public Uni<Response> create(PortfolioAccountMapping portfolioAccountMapping) {
        return PortfolioAccountMapping.save(client, portfolioAccountMapping.getPortfolioName(), portfolioAccountMapping.getRiskProfile(), portfolioAccountMapping.getAccountName())
                .onItem()
                .transform(name -> URI.create("/portfolio-account-mapping/" + name))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @Path("{portfolioName}/{accountName}")
    @POST
    public Uni<Response> addNewAccount(@PathParam("portfolioName") String portfolioName, @PathParam("accountName") String accountName) {
        return PortfolioAccountMapping.addAccount(client, portfolioName, accountName)
                .onItem()
                .transform(name -> URI.create("/portfolio-account-mapping/" + portfolioName + "/" + accountName))
                .onItem()
                .transform(uri -> Response.created(uri).build());
    }

    @DELETE
    @Path("{portfolioName}")
    public Uni<Response> delete(@PathParam("portfolioName") String portfolioName) {
        return PortfolioAccountMapping.delete(client, portfolioName)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    @DELETE
    @Path("{portfolioName}/{accountName}")
    public Uni<Response> deleteAccount(@PathParam("portfolioName") String portfolioName, @PathParam("accountName") String accountName) {
        return PortfolioAccountMapping.deleteAccount(client, portfolioName, accountName)
                .onItem()
                .transform(deleted -> deleted ? Response.Status.NO_CONTENT : Response.Status.NOT_FOUND)
                .onItem()
                .transform(status -> Response.status(status).build());
    }

    @GET
    @Path("{name}")
    public Uni<PortfolioAccountMapping> get(@PathParam("name") String name) {
        { return PortfolioAccountMapping.findByName(client, name); }
    }
}
