package org.acme;

import database.queries.InstrumentQueries;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;

public class Instrument {

    private String instrumentName;
    private Double instrumentValue;
    private Integer riskScore;

    public Instrument(String instrumentName, Double instrumentValue, Integer riskScore) {
        this.instrumentName = instrumentName;
        this.instrumentValue = instrumentValue;
        this.riskScore = riskScore;
    }
    public Instrument() {
    }

    public String getInstrumentName() {
        return instrumentName;
    }
    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public Double getInstrumentValue() {
        return instrumentValue;
    }
    public void setInstrumentValue(Double instrumentValue) {
        this.instrumentValue = instrumentValue;
    }

    public Integer getRiskScore() {
        return riskScore;
    }
    public void setRiskScore(Integer riskScore) {
        this.riskScore = riskScore;
    }

    public static Multi<Instrument> findAll(PgPool client) {
        return client.query(InstrumentQueries.selectAll()).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(Instrument::from);
    }

    public static Uni<Instrument> findByName(PgPool client, String instrumentName) {
        return client
                .preparedQuery(InstrumentQueries.findByName(instrumentName))
                .execute()
                .onItem()
                .transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }

    public static Uni<String> save(PgPool client, String instrumentName, Double instrumentValue, Integer riskScore) {
        return client
                .preparedQuery(InstrumentQueries.save(instrumentName, instrumentValue, riskScore))
                .execute()
                .onItem()
                .transform(m -> m.iterator().next().getString("instrument_name"));
    }

    public static Uni<Boolean> delete(PgPool client, String instrumentName) {
        return client
                .preparedQuery(InstrumentQueries.delete(instrumentName))
                .execute()
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    private static Instrument from(Row row) {
        return new Instrument(row.getString("instrument_name"), row.getDouble("instrument_value"), row.getInteger("risk_score"));
    }


}
