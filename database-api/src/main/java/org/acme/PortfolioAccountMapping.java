package org.acme;

import database.queries.PortfolioAccountMappingQueries;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;


public class PortfolioAccountMapping {

    private String portfolioName;
    private String riskProfile;
    private String[] accountName;

    public PortfolioAccountMapping(String portfolioName, String riskProfile, String[] accountName) {
        this.portfolioName = portfolioName;
        this.riskProfile = riskProfile;
        this.accountName = accountName;
    }


    public String getPortfolioName() {
        return portfolioName;
    }
    public void setPortfolioName(String portfolioName) {
        this.portfolioName = portfolioName;
    }

    public String getRiskProfile() {
        return riskProfile;
    }
    public void setRiskProfile(String riskProfile) {
        this.riskProfile = riskProfile;
    }

    public String[] getAccountName() {
        return accountName;
    }
    public void setAccountName(String[] accountName) {
        this.accountName = accountName;
    }

    public static Multi<PortfolioAccountMapping> findAll(PgPool client) {

        return client.query(PortfolioAccountMappingQueries.selectAll()).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(PortfolioAccountMapping::from);
    }

    public static Uni<PortfolioAccountMapping> findByName(PgPool client, String portfolioName) {
        return client
                .preparedQuery(PortfolioAccountMappingQueries.findByName(portfolioName))
                .execute()
                .onItem()
                .transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }

    public static Uni<String> save(PgPool client, String portfolioName, String riskProfile, String[] accountList) {
        return client
                .preparedQuery(PortfolioAccountMappingQueries.save())
                .execute(Tuple.of(portfolioName, riskProfile, accountList))
                .onItem()
                .transform(m -> m.iterator().next().getString("portfolio_name"));
    }

    public static Uni<String> addAccount(PgPool client, String portfolioName, String newAccount) {
        return client
                .preparedQuery(PortfolioAccountMappingQueries.addNewAccountToPortfolio(portfolioName, newAccount))
                .execute()
                .onItem()
                .transform(m -> {
                    m.rowCount();
                    return null;
                });
    }

    public static Uni<Boolean> delete(PgPool client, String portfolioName) {
        return client
                .preparedQuery(PortfolioAccountMappingQueries.delete(portfolioName))
                .execute()
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    public static Uni<Boolean> deleteAccount(PgPool client, String portfolioName, String accountName) {
        return client
                .preparedQuery(PortfolioAccountMappingQueries.deleteAccountFromPortfolio(portfolioName, accountName))
                .execute()
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    private static PortfolioAccountMapping from(Row row) {
        return new PortfolioAccountMapping(row.getString("portfolio_name"), row.getString("portfolio_risk_profile"), row.getArrayOfStrings("account_name"));
    }
}
