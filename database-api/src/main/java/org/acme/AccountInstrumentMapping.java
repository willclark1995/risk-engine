package org.acme;

import database.queries.AccountInstrumentMappingQueries;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;

public class AccountInstrumentMapping {

    private String accountName;
    private String instrumentName;
    private int instrumentQuantity;

    public AccountInstrumentMapping(String accountName, String instrumentName, Integer instrumentQuantity) {
        this.accountName = accountName;
        this.instrumentName = instrumentName;
        this.instrumentQuantity = instrumentQuantity;
    }

    public String getAccountName() {
        return accountName;
    }
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getInstrumentName() {
        return instrumentName;
    }
    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public Integer getInstrumentQuantity() {
        return instrumentQuantity;
    }
    public void setInstrumentQuantity(Integer instrumentQuantity) {
        this.instrumentQuantity = instrumentQuantity;
    }

    public static Multi<AccountInstrumentMapping> findAll(PgPool client) {

        return client.query(AccountInstrumentMappingQueries.selectAll()).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(AccountInstrumentMapping::from);
    }

    public static Uni<AccountInstrumentMapping> findByName(PgPool client, String accountName) {
        return client.query(AccountInstrumentMappingQueries.findByName(accountName)).execute()
                .onItem().transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }

    public static Uni<String> save(PgPool client, String accountName, String instrumentName, Integer instrumentQuantity) {
        return client
                .preparedQuery(AccountInstrumentMappingQueries.save(accountName, instrumentName, instrumentQuantity))
                .execute()
                .onItem()
                .transform(m -> m.iterator().next().getString("account_name"));
    }

    public static Uni<Boolean> delete(PgPool client, String accountName, String instrumentName) {
        return client
                .preparedQuery(AccountInstrumentMappingQueries.delete(accountName, instrumentName))
                .execute()
                .onItem()
                .transform(m -> m.rowCount() == 1);
    }

    private static AccountInstrumentMapping from(Row row) {
        return new AccountInstrumentMapping(row.getString("account_name"), row.getString("instrument_name"), row.getInteger("instrument_quantity"));
    }
}
