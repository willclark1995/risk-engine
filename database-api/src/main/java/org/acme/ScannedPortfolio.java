package org.acme;

import database.queries.ScanResultQueries;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;

public class ScannedPortfolio {

    private String portfolioName;
    private String status;
    private String description;
    private String date;

    public ScannedPortfolio(String portfolioName, String status, String description, String date) {
        this.portfolioName = portfolioName;
        this.status = status;
        this.description = description;
        this.date = date;
    }

    public ScannedPortfolio(){
        super();
    }

    public String getPortfolioName() {
        return portfolioName;
    }
    public void setPortfolioName(String portfolioName) {
        this.portfolioName = portfolioName;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public static Multi<ScannedPortfolio> findAll(PgPool client) {
        return client.query(ScanResultQueries.selectAll()).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ScannedPortfolio::from);
    }

    public static Uni<String> save(PgPool client, String query) {
        return client
                .preparedQuery(query)
                .execute()
                .onItem()
                .transform(m -> m.iterator().next().getString("portfolio_name"));
    }

    private static ScannedPortfolio from(Row row) {
        return new ScannedPortfolio(row.getString("portfolio_name"), row.getString("status"), row.getString("description"), row.getLocalDateTime("date_recorded").toString());
    }
}
