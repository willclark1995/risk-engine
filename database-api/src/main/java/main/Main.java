package main;

import database.requests.HttpRequestCreator;
import gson.PortfolioRiskProfilePair;
import io.vertx.mutiny.pgclient.PgPool;
import org.acme.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {

    private final String portfoliosEndpoint = "http://database-api:8080/portfolios";
    private final String riskProfilesEndpoint = "http://database-api:8080/risk-profile";
    private final String engineEndpoint = "http://risk-engine:8081/engine";
    public PgPool client;
    HttpRequestCreator htc = new HttpRequestCreator();

    public void setClient(PgPool client) {
        this.client = client;
    }

    public List<RiskProfile> fetchRiskProfiles() throws IOException { return Arrays.asList(htc.get(riskProfilesEndpoint, RiskProfile[].class)); }
    public List<Portfolio> fetchPortfolios() throws IOException { return Arrays.asList(htc.get(portfoliosEndpoint, Portfolio[].class)); }

    public boolean processPortfolios() throws IOException {

        List<RiskProfile> availableRiskProfiles = fetchRiskProfiles();
        List<Portfolio> returnedPortfolios = fetchPortfolios();

        return htc.post(engineEndpoint, new PortfolioRiskProfilePair(availableRiskProfiles, returnedPortfolios));
    }
}
