package database.requests;

import com.google.gson.Gson;
import com.squareup.okhttp.*;
import javax.enterprise.context.RequestScoped;
import java.io.IOException;

@RequestScoped
public class HttpRequestCreator {

    public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();
    Gson gson = new Gson();

    public <T> T get(String url, Class<T> responseClass) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = executor(client.newCall(request));
        return responseClass.cast(gson.fromJson(response.body().string(), responseClass));
    }

    public boolean post(String url, Object obj) throws IOException {
        String json = gson.toJson(obj);
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = executor(client.newCall(request));
        return response.isSuccessful();
    }

    public Response executor (Call call) throws IOException {
        return call.execute();
    }
}
