package database.queries;


import org.acme.ScannedPortfolio;

import java.util.List;

public interface ScanResultQueries {

    static String selectAll() {
        return "SELECT * FROM results";
    }

    static String insertResultsIntoTable(List<ScannedPortfolio> scannedPortfolios) {

        StringBuilder query = new StringBuilder("INSERT INTO results VALUES ");

        for (ScannedPortfolio scannedPortfolio : scannedPortfolios) {
            query.append(String.format(
                    "('%s', '%s', '%s'), ", scannedPortfolio.getPortfolioName(), scannedPortfolio.getStatus(), scannedPortfolio.getDescription())
            );
        }
        return query.toString().trim().replaceAll("[,]$", " returning portfolio_name;") ;
    }
}
