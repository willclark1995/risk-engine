package database.queries;

public interface PortfoliosQueries {

    static String getPortfolios() {
        return
                "WITH total_instrument_value AS (" +
                        "SELECT p.portfolio_name, a.account_name, a.instrument_name, i.instrument_value * instrument_quantity as total_instrument_value " +
                        "      FROM portfolio_account_mapping AS p" +
                        "      INNER JOIN account_instrument_mapping AS a ON a.account_name = ANY(p.account_name)" +
                        "      INNER JOIN instruments AS i ON i.instrument_name = a.instrument_name" +
                        ")," +
                        "total_account_values as " +
                        "(" +
                        "    SELECT portfolio_name, account_name, SUM(total_instrument_value) as total_account_value" +
                        "    FROM total_instrument_value" +
                        "    GROUP BY portfolio_name, account_name" +
                        ")," +
                        "total_portfolio_value as" +
                        "(" +
                        "    SELECT portfolio_name, SUM(total_account_value) as portfolio_value" +
                        "    FROM total_account_values" +
                        "    GROUP BY portfolio_name " +
                        ")," +
                        "" +
                        "instrument_value_as_percentage_of_portfolio AS (" +
                        "    SELECT p.portfolio_name, a.account_name, a.instrument_name, (a.instrument_quantity * i.instrument_value) / pv.portfolio_value as percentage_of_total_portfolio_value, i.risk_score" +
                        "    FROM portfolio_account_mapping p" +
                        "    INNER JOIN account_instrument_mapping a ON a.account_name = ANY(p.account_name)" +
                        "    INNER JOIN instruments AS i ON i.instrument_name = a.instrument_name" +
                        "    INNER JOIN total_portfolio_value pv on pv.portfolio_name = p.portfolio_name" +
                        ")" +
                        "SELECT p.portfolio_name, p2.portfolio_risk_profile, sum(p.percentage_of_total_portfolio_value * p.risk_score) as portfolio_risk_score        " +
                        "                                        FROM instrument_value_as_percentage_of_portfolio p            " +
                        "                                        INNER JOIN portfolio_account_mapping p2 ON p.portfolio_name = p2.portfolio_name          " +
                        "                                        GROUP BY p.portfolio_name, p2.portfolio_risk_profile;";
    }
}
