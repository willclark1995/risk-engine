package database.queries;

public interface AccountInstrumentMappingQueries {

    static String selectAll() {
        return "SELECT * FROM account_instrument_mapping";
    }

    static String findByName(String accountName) {
        return String.format("SELECT * FROM account_instrument_mapping WHERE account_name = '%s'", accountName);
    }

    static String save(String accountName, String instrumentName, Integer instrumentQuantity) {
        return String.format("INSERT INTO account_instrument_mapping (account_name, instrument_name, instrument_quantity) VALUES ('%s', '%s', '%s') RETURNING account_name", accountName, instrumentName, instrumentQuantity);
    }

    static String delete(String accountName, String instrumentName) {
        return String.format("DELETE FROM account_instrument_mapping WHERE account_name = '%s' AND instrument_name = '%s'", accountName, instrumentName);
    }
}
