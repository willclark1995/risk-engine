package database.queries;

public interface RiskProfileQueries {

    static String selectAll() {
        return "SELECT * FROM risk_profiles";
    }

    static String findByName(String riskProfileName) {
        return String.format("SELECT * FROM risk_profiles WHERE risk_profile = '%s'", riskProfileName);
    }

    static String save(String riskProfile, String riskProfileDescription, Double minimumRiskLimit, Double maximumRiskLimit, Double warningThresholdPercentage) {
        return String.format("INSERT INTO risk_profiles (risk_profile, risk_profile_description, minimum_risk_limit, maximum_risk_limit, warning_threshold_percentage) VALUES ('%s', '%s', '%s', '%s', '%s') RETURNING risk_profile",
                riskProfile, riskProfileDescription, minimumRiskLimit, maximumRiskLimit, warningThresholdPercentage);
    }

    static String delete(String riskProfileName) {
        return String.format("DELETE FROM risk_profiles WHERE risk_profile = '%s'", riskProfileName);
    }
}
