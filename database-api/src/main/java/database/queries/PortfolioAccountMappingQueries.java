package database.queries;

public interface PortfolioAccountMappingQueries {

    static String selectAll() {
        return "SELECT * FROM portfolio_account_mapping";
    }

    static String findByName(String portfolioName) {
        return String.format("SELECT * FROM portfolio_account_mapping WHERE portfolio_name = '%s'", portfolioName);
    }

    static String save() {
        return "INSERT INTO portfolio_account_mapping (portfolio_name, portfolio_risk_profile, account_name) VALUES ($1, $2, $3) RETURNING portfolio_name";
    }

    static String delete(String portfolioName) {
        return String.format("DELETE FROM portfolio_account_mapping WHERE portfolio_name = '%s'", portfolioName);
    }

    static String addNewAccountToPortfolio(String portfolioName, String newAccount) {
        return String.format("UPDATE portfolio_account_mapping SET account_name = array_append(account_name, '%s') WHERE portfolio_name = '%s'", newAccount, portfolioName);
    }

    static String deleteAccountFromPortfolio(String portfolioName, String accountName) {
        return String.format("update portfolio_account_mapping set account_name = array_remove(account_name, '%s') WHERE portfolio_name = '%s'", accountName, portfolioName);
    }
}
