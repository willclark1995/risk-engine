package database.queries;

public interface InstrumentQueries {

    static String selectAll() {
        return "SELECT * FROM instruments";
    }

    static String findByName(String instrumentName) {
        return String.format("SELECT * FROM instruments WHERE instrument_name = '%s'", instrumentName);
    }

    static String save(String instrumentName, Double instrumentValue, Integer riskScore) {
        return String.format("INSERT INTO instruments (instrument_name, instrument_value, risk_score) VALUES ('%s', '%s', '%s') RETURNING instrument_name", instrumentName, instrumentValue, riskScore);
    }

    static String delete(String instrumentName) {
        return String.format("DELETE FROM instruments WHERE instrument_name = '%s'", instrumentName);
    }

}
