package gson;

import org.acme.Portfolio;
import org.acme.RiskProfile;

import java.util.List;

public class PortfolioRiskProfilePair {

    private java.util.List<RiskProfile> riskProfiles;
    private List<Portfolio> portfolios;

    public PortfolioRiskProfilePair(List<RiskProfile> riskProfiles, List<Portfolio> portfolios) {
        this.riskProfiles = riskProfiles;
        this.portfolios = portfolios;
    }

    public List<RiskProfile> getRiskProfiles() {
        return riskProfiles;
    }
    public void setRiskProfiles(List<RiskProfile> riskProfiles) {
        this.riskProfiles = riskProfiles;
    }

    public List<Portfolio> getPortfolios() {
        return portfolios;
    }
    public void setPortfolios(List<Portfolio> portfolios) {
        this.portfolios = portfolios;
    }
}
