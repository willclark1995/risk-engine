package gson;


import org.acme.ScannedPortfolio;

import java.util.List;

public class ScannedPortfolios {

    public List<ScannedPortfolio> scannedPortfolios;

    public ScannedPortfolios(){
        super();
    }

    public ScannedPortfolios(List<ScannedPortfolio> scannedPortfolios) {
        this.scannedPortfolios = scannedPortfolios;
    }

    public List<ScannedPortfolio> getPortfolioResults() {
        return scannedPortfolios;
    }
    public void setPortfolioResults(List<ScannedPortfolio> scannedPortfolios) {
        this.scannedPortfolios = scannedPortfolios;
    }
}
