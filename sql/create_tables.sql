CREATE TABLE IF NOT EXISTS portfolio_account_mapping (
  portfolio_name text NOT NULL,
  portfolio_risk_profile text NOT NULL,
  account_name text[],
  PRIMARY KEY (portfolio_name)
);

CREATE TABLE IF NOT EXISTS account_instrument_mapping (
  account_name text NOT NULL,
  instrument_name text,
  instrument_quantity integer
);

CREATE TABLE IF NOT EXISTS instruments (
  instrument_name text NOT NULL,
  instrument_value decimal(9,2) NOT NULL,
  risk_score integer NOT NULL,
  PRIMARY KEY (instrument_name)
);

CREATE TABLE IF NOT EXISTS risk_profiles (
  risk_profile text NOT NULL,
  risk_profile_description text NOT NULL,
  minimum_risk_limit decimal(4,2) NOT NULL,
  maximum_risk_limit decimal(4,2) NOT NULL,
  warning_threshold_percentage decimal(4,3) NOT NULL,
  PRIMARY KEY (risk_profile)
);

CREATE TABLE IF NOT EXISTS results (
  portfolio_name text NOT NULL,
  status text NOT NULL,
  description text NOT NULL,
  date_recorded timestamp default current_timestamp
);