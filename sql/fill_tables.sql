INSERT INTO public.portfolio_account_mapping
VALUES 
	('POR_1', 'L', '{"ACC_1", "ACC_2"}'),
	('POR_2', 'L', '{"ACC_2", "ACC_3"}'),
	('POR_3', 'L', '{"ACC_3", "ACC_4"}'),
	('POR_4', 'L', '{"ACC_4", "ACC_5"}'),
	('POR_5', 'L', '{"ACC_5", "ACC_6"}'),
	('POR_6', 'M', '{"ACC_1", "ACC_4"}'),
	('POR_7', 'M', '{"ACC_1", "ACC_5"}'),
	('POR_8', 'M', '{"ACC_1", "ACC_6"}'),
	('POR_9', 'M', '{"ACC_2", "ACC_4"}'),
	('POR_10', 'M', '{"ACC_2", "ACC_5"}'),
	('POR_11', 'M', '{"ACC_2", "ACC_6"}'),
	('POR_12', 'M', '{"ACC_3", "ACC_5"}'),
	('POR_13', 'H', '{"ACC_3", "ACC_6"}'),
	('POR_14', 'H', '{"ACC_1", "ACC_2", "ACC_3"}'),
	('POR_15', 'H', '{"ACC_1", "ACC_3", "ACC_5"}'),
	('POR_16', 'H', '{"ACC_1", "ACC_2", "ACC_5", "ACC_6"}');
	
	
INSERT INTO public.account_instrument_mapping
VALUES 
	('ACC_1', 'A', 5),
	('ACC_1', 'B', 13),
	('ACC_1', 'C', 6),
	('ACC_2', 'D', 25),
	('ACC_2', 'E', 3),
	('ACC_2', 'F', 9),
	('ACC_3', 'A', 15),
	('ACC_3', 'C', 8),
	('ACC_3', 'E', 8),
	('ACC_4', 'B', 50),
	('ACC_4', 'E', 21),
	('ACC_4', 'F', 8),
	('ACC_5', 'D', 5),
	('ACC_5', 'E', 5),
	('ACC_5', 'F', 4),
	('ACC_6', 'A', 13),
	('ACC_6', 'E', 21),
	('ACC_6', 'F', 17);
	
INSERT INTO public.instruments
VALUES 
	('A', 12, 6),
	('B', 10, 3),
	('C', 20, 9),
	('D', 15, 2),
	('E', 6, 4),
	('F', 9.50, 4);
	
INSERT INTO public.risk_profiles
VALUES 
	('L', 'Low', 0, 3, .3),
	('M', 'Medium', 3, 6, .1),
	('H', 'High', 6, 9, .2);